#!/usr/bin/env python

# wav2img.py: Module for converting a wav (or any file, really) into an image.
#     v.2011.01.27.13.37

# Copyright 2011 by Andreas Skyman, http://www.dd.chalmers.se/~skymandr/
# You may use and distribute this file under the terms of the GPL.

from pylab import *
import scipy


def read_data(filename):
    """Read a (wav) file and converts chars to ints. Returns array of ints."""

    with open(filename, 'rb') as f:
        data_str = f.read()
    
    data = zeros(len(data_str))
    for i, x in enumerate(data_str):
        data[i] = ord(data_str[i])

    return data
        

def make_imagedata(xsize, ysize, data):
    """Reshapes data from read_data into a rectangle of desired shape, padding 
    with zeros if necessary. Also inserts 'key'."""

    try:
        imagedata = hstack( (data, hstack( 
                    (zeros(xsize*ysize - size(data) - 256), range(256)) )) )
    except ValueError:
        print 'Error! Image size must be >= data-size + 256!'
        print ' Size of data: ' + `size(data)`
        print ' Image pixels: ' + `xsize*ysize`
        print ' Difference:   ' +  `size(data) - xsize*ysize`
        return -1

    imagedata = reshape(imagedata, (ysize, xsize))
    print 'Size of data: ' + `size(data)`
    print 'Image pixels: ' + `xsize*ysize`
    print '0:s and key:  ' + `xsize*ysize - size(data)`

    return imagedata

    
def make_red(imagedata):
    """Converts imagedata from make_imagedata to pylab's RGB-format, using only 
    the red channel."""

    ymax, xmax = shape(imagedata)
    redimage = zeros( (ymax, xmax, 3) )   

    for x in range(xmax):
        for y in range(ymax):
            redimage[y, x, 0] = imagedata[y, x]
    
    return int_(redimage)

def make_RGB(imagedata):
    """Converts imagedata from make_imagedata to pylab's RGB-format, using all 
    four channels separately + one for greya dn one for yellow."""

    ymax, xmax = shape(imagedata)
    RGBimage = zeros( (ymax, xmax, 3) )   

    for x in range(xmax):
        for y in range(ymax):
            if imagedata[y, x] < 80:
                RGBimage[y, x, 0:3] = 18 + 3*imagedata[y, x]*ones(3)
            elif imagedata[y, x] < 113:
                RGBimage[y, x, 2] = 63 + 6*(imagedata[y, x] - 80)
            elif imagedata[y, x] < 146:
                RGBimage[y, x, 0] = 63 + 6*(imagedata[y, x] - 113)
            elif imagedata[y, x] < 179:
                RGBimage[y, x, 1] = 63 + 6*(imagedata[y, x] - 146)
            else:
                RGBimage[y, x, 0:2] = 27 + 3*(imagedata[y, x] - 179)*ones(2)
#            RGBimage[y, x, -1] = 255 
        
    return int_(RGBimage)


def save_image(filename, imagedata):
    """Saves image to file. (Works best on PNG-images...)"""

    scipy.misc.imsave(filename, imagedata)


def decode_red(imagefile, wavfile=''):
    """This sample decoder loads and decodes and image made using make_red. It 
    saves to file if wavefile is specified. It works on similar encodings as 
    well, but results may be distorted.
    
    If you are attempting to read e.g. a scanned or otherwise reprocessed
    image, here are some things you may want to consider:
        * At the bottom right, you will find 256 pixels that are the 'key' to
        the encoding. They simply represent numbers 0 through 255. For the 
        make_red encoding, this will appear as a smooth gradient from black to
        bright red.
        * Try to make sure your image is as many pixels as the original file, 
        or sound will be distorted.
        * Your byte conversion is likely to be slightly imperfect, hence normal
        audioplayers may not recognise the result as a wav-file. To get around
        this, you can import it as 'raw data' in e.g. Audacity. For this to
        work, you need to know the type and sample frequency of the original 
        file,  at least approximately. 8 bit PCM at 8000Hz, mono is what I use. 
        * If you are close to the original settings, you may still get somthing 
        intelligible -- try it out for fun!

    Returns image and imagestring. (Works best on PNG-images, other formats may 
    be loaded 'backwards' so you will have to flip the matrix before exporting 
    to wav. Identifying the 'key' mentioned above in the imported array will 
    help you determine the correct orientation.)"""
    
    imagedata = imread(imagefile)[:, :, 0]
    if imagedata.dtype.name == 'float32':
        imagedata *= 255

    imagestring = ''
    for n in reshape(imagedata, size(imagedata)):
        imagestring += chr(int(round(n)))
 
    if wavfile:
        with open(wavfile, 'wb') as f:
            f.write(imagestring[0:-256])

    return imagedata, imagestring

def decode_RGB(imagefile, wavfile=''):
    """This sample decoder loads and decodes and image made using make_RGB. It 
    saves to file if wavefile is specified. It works on similar encodings as 
    well, but results may be distorted.
    
    If you are attempting to read e.g. a scanned or otherwise reprocessed
    image, here are some things you may want to consider:
        * At the bottom right, you will find 256 pixels that are the 'key' to
        the encoding. They simply represent numbers 0 through 255. For the 
        make_red encoding, this will appear as a smooth gradient from black to
        bright red.
        * Try to make sure your image is as many pixels as the original file, 
        or sound will be distorted.
        * Your byte conversion is likely to be slightly imperfect, hence normal
        audioplayers may not recognise the result as a wav-file. To get around
        this, you can import it as 'raw data' in e.g. Audacity. For this to
        work, you need to know the type and sample frequency of the original 
        file,  at least approximately. 8 bit PCM at 8000Hz, mono,is what I use.
        * If you are close to the original settings, you may still get somthing 
        intelligible -- try it out for fun!

    Returns image and imagestring. (Works best on PNG-images, other formats may 
    be loaded 'backwards' so you will have to flip the matrix before exporting 
    to wav. Identifying the 'key' mentioned above in the imported array will 
    help you determine the correct orientation.)"""

    imagedata = imread(imagefile)[:, :, 0:3]
    if imagedata.dtype.name == 'float32':
        imagedata *= 255

    imagestring = ''
    for n in reshape(imagedata, (size(imagedata[:,:,0]), 3)):
        if n[0]:
            if n[1]:
                if n[2]:
                    N = (n[0:3].mean() - 18)/3.0
                else:
                    N = (n[0:2].mean() - 27)/3.0 + 179
            else:
                N = (n[0] - 63)/6.0 + 113
        elif n[1]:
            N = (n[1] - 63)/6.0 + 146
        else:
            N = (n[2] - 63)/6.0 + 80
                
        imagestring += chr(int(round(N)))
 
    if wavfile:
        with open(wavfile, 'wb') as f:
            f.write(imagestring[0:-256])

    return imagedata, imagestring


def decode_scan(imagefile, wavfile=''):
    """This decoder is meant to work on any image made using the routines in
    this module, by ysing the supplied key as look-up table. It is meant to do
    an acceptable job, even with scanned images. The result is saved to file
    if _wavfile_ is specified.
    
    If you are attempting to read e.g. a scanned or otherwise reprocessed
    image, here are some things you may want to consider:
        * At the bottom right, you will find 256 pixels that are the 'key' to
        the encoding. They simply represent numbers 0 through 255. For the 
        make_red encoding, this will appear as a smooth gradient from black to
        bright red.
        * Try to make sure your image is as many pixels as the original file, 
        or sound will be distorted.
        * Your byte conversion is likely to be slightly imperfect, hence normal
        audioplayers may not recognise the result as a wav-file. To get around
        this, you can import it as 'raw data' in e.g. Audacity. For this to
        work, you need to know the type and sample frequency of the original 
        file,  at least approximately. 8 bit PCM at 8000Hz, mono,is what I use.
        * If you are close to the original settings, you may still get somthing 
        intelligible -- try it out for fun!

    Returns image, imagestring and key. (Works best on PNG-images, other formats
    may be loaded 'backwards' so you will have to flip the matrix before 
    exporting to wav. Identifying the 'key' mentioned above in the imported 
    array will help you determine the correct orientation.)"""

    imagedata = imread(imagefile)[:, :, 0:3]
    if imagedata.dtype.name == 'float32':
        imagedata *= 255

    imagestring = ''
    key = reshape(imagedata, (size(imagedata[:,:,0]), 3))[-256:]
    for n in reshape(imagedata, (size(imagedata[:,:,0]), 3)):
        diffs = ((key - n)**2).sum(axis=1)
        imagestring += chr(find(diffs == diffs.min())[0])

    if wavfile:
        with open(wavfile, 'wb') as f:
            f.write(imagestring[0:-256])

    return imagedata, imagestring, key
